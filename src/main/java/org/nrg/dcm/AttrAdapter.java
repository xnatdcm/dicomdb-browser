/*
 * Copyright (c) 2006-2012 Washington University
 */
package org.nrg.dcm;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.dcm4che2.util.TagUtils;
import org.nrg.attr.AbstractAttrAdapter;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrDef.Optional;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.nrg.attr.ReadableAttrDefSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

/**
 * For a given FileSet, generates external attributes from the corresponding
 * DICOM fields.
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class AttrAdapter extends AbstractAttrAdapter<Integer,String> {
    private final Logger logger = LoggerFactory.getLogger(AttrAdapter.class);
    private final FileSet fs;

    /**
     * Creates a new attribute adapter for the given FileSet
     * @param fs DICOM FileSet
     * @param attrs AttributeSets for conversion
     */
    public AttrAdapter(final FileSet fs, final ReadableAttrDefSet<Integer,String>...attrs) {
        super(new AttrDefSet(), attrs);
        this.fs = fs;
    }

    public Collection<Map<Integer,String>>
    getUniqueCombinationsGivenValues(final Map<Integer,String> given,
            final Collection<Integer> attrs,
            final Map<Integer,ConversionFailureException> failures)
            throws ExtAttrException {
        try {
            return fs.getUniqueCombinationsGivenValues(given, attrs, failures);
        } catch (IOException e) {
            throw new ExtAttrException(e);
        } catch (SQLException e) {
            throw new ExtAttrException(e);
        }
    }

    /**
     * For each of the named files, returns the single value of each specified attribute
     * @param files files to be checked
     * @return map from each file to a map from attribute name to value
     * @throws IOException
     * @throws NoUniqueValueException
     * @throws ExtAttrConversionException
     */
    public ListMultimap<File,ExtAttrValue> getValuesForFiles(final File...files)
    throws IOException,SQLException {
        final ListMultimap<File,ExtAttrValue> values = ArrayListMultimap.create();

        final Map<Integer,ConversionFailureException> failedDICOM = Maps.newLinkedHashMap();
        final Set<Integer> tags = getDefs().getNativeAttrs();
        fs.fillCache(tags, failedDICOM);

        for (final File file : files) {
            ATTRS: for (final ExtAttrDef<Integer,String> ea : getDefs()) {
                final Multimap<Integer,String> dcmVals = fs.getUniqueValuesFromFiles(Collections.singleton(file), tags, failedDICOM);

                final Map<Integer,String> singDcmVals = Maps.newHashMap();
                for (final int tag : dcmVals.keySet()) {
                    final Iterator<String> valsi = dcmVals.get(tag).iterator();
                    if (!valsi.hasNext()) {
                        if (ea.requires(tag)) {
                            // TODO: export this failure
                            if (logger.isInfoEnabled()) {
                                final StringBuilder sb = new StringBuilder("DICOM attribute ");
                                sb.append(TagUtils.toString(tag));
                                sb.append(" is missing in ").append(file);
                                sb.append("; required for ").append(ea);
                                logger.info(sb.toString());
                            }
                            continue ATTRS;
                        }
                    } else {
                        final String val = valsi.next();
                        if (valsi.hasNext()) {
                            throw new RuntimeException("multiple DICOM attribute values in single file?");
                        } else {
                            singDcmVals.put(tag, val);
                        }
                    }
                }
                try {
                    values.put(file, ea.convert(singDcmVals));
                } catch (ConversionFailureException e) {
                    if (ea instanceof Optional) {
                        // no problem, just skip it
                    } else {
                        // TODO: export this failure
                        final StringBuilder sb = new StringBuilder("Unable to build attribute ");
                        sb.append(ea).append(" from file ").append(file);
                        logger.warn(sb.toString(), e);
                    }
                }
            }
        }

        //    } catch (ConversionFailureException e) {
        //      final Set<String> failedAttrs = new HashSet<String>();
        //      for (final ExtAttrDef<Integer,String> ea : getDefs()) {
        //        if (ea.getAttrs().contains(e.getAttr()))
        //          failedAttrs.add(ea.getName());
        //      }
        //      throw new ConversionFailureException(e, failedAttrs.toArray(new String[0]));
        //    }

        return values;
    }
}
