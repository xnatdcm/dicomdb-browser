/**
 * Copyright (c) 2006-2010 Washington University
 */
package org.nrg.dcm;

import java.io.File;
import java.io.IOException;
import java.lang.RuntimeException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.Set;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.SQLWarning;

import org.apache.commons.lang.StringEscapeUtils;

import org.dcm4che2.data.Tag;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.UID;
import org.dcm4che2.media.DicomDirReader;
import org.dcm4che2.net.TransferCapability;
import org.dcm4che2.util.StringUtils;

import org.nrg.attr.ConversionFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;


/**
 * Finds DICOM files in a directory and manages access to attribute values
 * held in those files.
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public final class FileSet {
    private static final String PROP_RETAIN_DB = "org.nrg.DicomDB.retain-db";
    private static final String DICOMDIR = "DICOMDIR";
    private static final String DBPREFIX = "org.nrg.dcm";
    private static final String DBSUFFIX = "-db";

    // Default Transfer Syntax UID (PS 3.5, section 10)
    private static final String DEFAULT_TS_UID = UID.ImplicitVRLittleEndian;

    // String format used to construct column names from tags
    private static final String COLUMN_FORMAT = "%1$s%2$08x";

    private final DirectoryRecord.Factory factory;
    private final Set<DirectoryRecord> patients;
    private final SortedSet<Integer> recordTags = Sets.newTreeSet();

    private final Logger logger = LoggerFactory.getLogger(FileSet.class);

    private final Connection db;
    private final File dbf;
    private static final String tableName = "Attributes";
    private static final List<File> dbFiles = Lists.newArrayList();
    private final String CREATE_TABLE_OPTION = "table";


    private static final int NO_MAX_VALUE = -1;
    private static int maxValueLength = NO_MAX_VALUE;
    private String valueTruncateFormat = "%1$s...(truncated)";

    private final Map<File,int[]> declaredComplete = Maps.newHashMap();

    private final SortedMap<Integer,String> tagToCol = Maps.newTreeMap();

    private final Set<File> roots = Sets.newLinkedHashSet();

    private final PreparedStatement qCount, qGetAllPaths, qGetOnePath, qPathTestRow, qPathAddRow, qPathDeleteRow;


    /**
     * Create a FileSet without building an explicit representation of directory entries
     */
    public FileSet(final File[] files) throws IOException,SQLException {
        this(files, null, null, null);
    }

    public FileSet(final File file) throws IOException,SQLException {
        this(new File[]{file}, null, null, null);
    }

    public FileSet(final File[] files, final Map<String,String> options)
    throws IOException,SQLException {
        this(files, options, null, null);
    }

    public FileSet(final File file, final Map<String,String> options)
    throws IOException,SQLException {
        this(new File[]{file}, options, null, null);
    }

    /**
     * @param buildRecords true if directory entry records should be built using default types
     */
    public FileSet(final File[] files, final Map<String,String> options,
            final boolean buildRecords, final ProgressMonitorI pn)
    throws IOException,SQLException {
        this(files, options, buildRecords ? DirectoryRecord.getDefaultFactory() : null, pn);
    }

    public FileSet(final File file, final Map<String,String> options,
            final boolean buildRecords, final ProgressMonitorI pn)
    throws IOException,SQLException {
        this(new File[]{file}, options, buildRecords, pn);
    }

    public FileSet(final File[] files, final boolean buildRecords, final ProgressMonitorI pn)
    throws IOException,SQLException {
        this(files, null, buildRecords, pn);
    }

    public FileSet(final File file, final boolean buildRecords, final ProgressMonitorI pn)
    throws IOException,SQLException {
        this(new File[]{file}, null, buildRecords, pn);
    }

    public FileSet(final File file, final boolean buildRecords)
    throws IOException,SQLException {
        this(new File[]{file}, null, buildRecords, null);
    }

    public FileSet(final File file, final Map<String,String> options,
            final DirectoryRecord.Factory recordFactory, final ProgressMonitorI pn)
    throws IOException,SQLException {
        this(new File[]{file}, options, recordFactory, pn);
    }

    public FileSet(final File file, final Map<String,String> options,
            final DirectoryRecord.Factory recordFactory)
    throws IOException,SQLException {
        this(new File[]{file}, options, recordFactory, null);
    }

    /**
     * @param files List of files and/or directories to be included in FileSet
     * @param options map of option name and values; valid options are "table", which specifies
     *    a qualifier (e.g., CACHED) to be added to the CREATE TABLE command; and all hsqldb options.
     *    If you don't know what options to set, you probably don't need to set any.
     * @param factory Factory object for Directory Record Entries; if null, no record entries are created
     * @throws IOException
     */
    public FileSet(final File[] files, Map<String,String> options,
            final DirectoryRecord.Factory recordFactory, final ProgressMonitorI pn)
    throws IOException,SQLException {
        factory = recordFactory;

        recordTags.add(Tag.SOPClassUID);            // need these two tags for transfer capabilities;
        recordTags.add(Tag.TransferSyntaxUID);      // also, DataSetAttrs constructor needs at least one tag

        if (null == factory) {
            patients = null;
        } else {
            patients = Sets.newLinkedHashSet();
            for (DirectoryRecord.Type type : DirectoryRecord.Type.getTypes()) {
                recordTags.addAll(factory.getSelectionKeys(type));
            }
        }

        if (null == options) {
            options = Collections.emptyMap();
        }

        final StringBuilder createTable = new StringBuilder("CREATE ");
        if (options.containsKey(CREATE_TABLE_OPTION)) {
            createTable.append(options.get(CREATE_TABLE_OPTION));
            createTable.append(" ");
            options.remove(CREATE_TABLE_OPTION);      // don't pass this to hsqldb
        }
        createTable.append("TABLE ");
        createTable.append(tableName);
        createTable.append(" ( path VARCHAR PRIMARY KEY, sop_class_uid VARCHAR, transfer_syntax_uid VARCHAR );");

        //  don't need this file, but we use the name
        dbf = File.createTempFile(DBPREFIX, DBSUFFIX);
        dbf.deleteOnExit(); // add delete on exit in case this thread dies

        final StringBuilder sb = new StringBuilder("jdbc:hsqldb:file:");
        sb.append(dbf.getPath());
        for (String opt : options.keySet()) {
            sb.append(";");
            sb.append(opt);
            sb.append("=");
            sb.append(options.get(opt));
        }

        final String user = "sa";
        final String password = "";

        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        db = DriverManager.getConnection(sb.toString(), user, password);

        final Statement statement = db.createStatement();
        statement.execute(createTable.toString());
        statement.close();

        qCount = db.prepareStatement("SELECT COUNT(*) FROM " + tableName);
        qGetOnePath = db.prepareStatement("SELECT TOP 1 path FROM " + tableName);
        qGetAllPaths = db.prepareStatement("SELECT path FROM " + tableName);

        qPathTestRow = db.prepareStatement("SELECT path FROM " + tableName + " WHERE path=?");
        qPathAddRow = db.prepareStatement("INSERT INTO " + tableName + " ( path, sop_class_uid, transfer_syntax_uid ) VALUES ( ?, ?, ? )");
        qPathDeleteRow = db.prepareStatement("DELETE FROM " + tableName + " WHERE path=?");

        if (!"true".equals(System.getProperty(PROP_RETAIN_DB))) {
            for (final String suffix : new String[]{"", ".properties", ".script", ".data", ".backup", ".log", ".lck"}) {
                final File f = new File(dbf.getPath() + suffix);
                dbFiles.add(f);
                f.deleteOnExit();
            }
        }

        // findDataFiles() must come after DirectoryRecord factory configuration
        findDataFiles(files, pn);
        if (null != pn ) {
            pn.close();
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(final Object o) {
        try {
            return o instanceof FileSet && getDataFiles().equals(((FileSet)o).getDataFiles());
        } catch (SQLException e) {
            return this == o;
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        try {
            return getDataFiles().hashCode();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void add(final File[] files, final ProgressMonitorI pn)
    throws IOException,SQLException {
        findDataFiles(files, pn);
    }

    public void add(final File[] files)
    throws IOException,SQLException {
        add(files, null);
    }


    private boolean contains(final File f) throws SQLException {
        final ResultSet rs = qPathTestRow.executeQuery();
        try {
            return rs.next();
        } finally {
            rs.close();
        }
    }

    /**
     * Remove the given Files from this fileset.
     * @param fs File records to be removed from the fileset
     * @return number of file records removed
     */
    public int remove(final File...fs) {
        int nRemoved = 0;
        for (final File f : fs) {
            try {
                final File cf = f.getCanonicalFile();
                qPathDeleteRow.setString(1, cf.getPath());
                nRemoved += qPathDeleteRow.executeUpdate();
            } catch (SQLException e) {
                logger.error("Unable to remove " + f + " from file set", e);
            } catch (IOException e) {
                logger.error("Unable to remove " + f + " from file set", e);
            }
        }
        return nRemoved;
    }


    /**
     * Remove the referenced files for the given directory records from this fileset.
     * @param rs Collection of directory records to be removed
     * @return Files removed from the fileset
     */
    public Collection<File> remove(final Collection<DirectoryRecord> rs) {
        final Set<String> pathnames = Sets.newLinkedHashSet();

        for (final DirectoryRecord patient : patients)
            pathnames.addAll(patient.purge(rs));

        final Set<File> removed = Sets.newHashSet();

        for (final String pathname : pathnames) {
            final File file = new File(pathname);
            try {
                qPathDeleteRow.setString(1, pathname);
                qPathDeleteRow.executeUpdate();
                removed.add(file);
            } catch (SQLException e) {
                logger.error("Unable to remove " + file.getPath() + " from file set", e);
            }
        }

        return removed;
    }


    public void dispose() {
        try {
            final Statement s = db.createStatement();
            try {
                if ("true".equals(System.getProperty(PROP_RETAIN_DB))) {
                    s.execute("SHUTDOWN IMMEDIATELY");
                } else {
                    s.execute("SHUTDOWN");
                }
            } finally {
                s.close();
            }
            db.close();
        } catch (SQLException e) {
            logger.error("Could not close database: " + e.getMessage());
        }
        for (final File f : dbFiles) {
            f.delete();  // these should be deleted at exit, but we can do it now
        }
    }


    /**
     * Sets the maximum length of attribute values, in characters.
     * Any values longer than this are truncated, with a truncation message appended.
     * @param m   maximum length; must be greater than zero.
     */
    public void setMaxValueLength(final int m) {
        if (m <= 0) {
            throw new IllegalArgumentException("max attribute value length must be > 0");
        }
        maxValueLength = m;
    }

    /**
     * Specifies that the full, original length of attributes is to be used.
     *
     */
    public void setMaxValueLength() { maxValueLength = NO_MAX_VALUE; }

    /**
     * Sets the message appended to attribute values that have been truncated.
     * @param msg message appended to values to indicate truncation
     */
    public void setTruncateFormat(final String msg) { valueTruncateFormat = msg; }


    /**
     * Get the root of the tree of directory record entries for this FileSet
     * @return The root entry DirectoryRecord, or null if no DirectoryRecord factory was provided
     * to the FileSet constructor.
     */
    public Set<DirectoryRecord> getPatientDirRecords() {
        return patients == null ? null : Collections.unmodifiableSet(patients);
    }


    /**
     * @return Set of all data files in this file set
     */
    public Set<File> getDataFiles() throws SQLException {
        final ResultSet rs = qGetAllPaths.executeQuery();
        try {
            final Set<File> files = Sets.newLinkedHashSet();
            while (rs.next()) {
                files.add(new File(rs.getString(1)));
            }
            return files;
        } finally {
            rs.close();
        }
    }


    /**
     * @return Number of data files in this file set
     */
    public int size() throws SQLException {
        final ResultSet rs = qCount.executeQuery();
        try {
            rs.next();
            return rs.getInt(1);
        } finally {
            rs.close();
        }
    }

    /*
     * @return true if there are any data files in this file set
     * @throws SQLException
     */
    public boolean isEmpty() throws SQLException {
        final ResultSet rs = qGetOnePath.executeQuery();
        try {
            return !rs.next();
        } finally {
            rs.close();
        }
    }


    /**
     * Returns the capabilities required to transfer the files in this FileSet
     * @param role Role of the caller in the transfer (TransferCapability.SCU or .SCP)
     * @return array of TransferCapability 
     */
    public TransferCapability[] getTransferCapabilities(final String role)
    throws SQLException {
        final Statement s = db.createStatement();
        try {
            final ResultSet rs = s.executeQuery("SELECT DISTINCT sop_class_uid, transfer_syntax_uid FROM " + tableName);
            try {
                return extractTransferCapabilities(role, rs);
            } finally {
                rs.close();
            }
        } finally {
            s.close();
        }
    }


    public TransferCapability[] getTransferCapabilities(final String role, final Collection<File> files)
    throws SQLException {
        final StringBuilder sb = new StringBuilder("SELECT DISTINCT sop_class_uid, transfer_syntax_uid FROM ");
        sb.append(tableName).append(" WHERE path IN ('");
        Joiner.on("','").appendTo(sb, files);
        sb.append("')");

        final Statement s = db.createStatement();
        try {
            final ResultSet rs = s.executeQuery(sb.toString());
            try {
                return extractTransferCapabilities(role, rs);
            } finally {
                rs.close();
            }
        } finally {
            s.close();
        }
    }


    private static TransferCapability[] extractTransferCapabilities(final String role, final ResultSet rs)
    throws SQLException {
        final Multimap<String,String> tcmap = LinkedHashMultimap.create();
        while (rs.next()) {
            tcmap.put(rs.getString(1), rs.getString(2));
        }

        final List<TransferCapability> tcs = Lists.newArrayList();
        for (final String k : tcmap.keySet()) {
            tcs.add(new TransferCapability(k, tcmap.get(k).toArray(new String[0]), role));
        }
        return tcs.toArray(new TransferCapability[0]);
    }


    /**
     * @return Set of "roots" for this file set, i.e., all directories that were specified
     * as containing this file set, plus the parent directories of all explicitly specified files 
     */
    public Set<File> getRoots() {
        return Collections.unmodifiableSet(roots);
    }

    /**
     * Walks the entries of a DICOMDIR file set to extract the image file information.
     */
    private synchronized Collection<File> readFileSetRecords(final File dir, final DicomDirReader dcd,
            final DicomObject fsRecord, final DirectoryRecord upper)
            throws IOException {
        final List<File> files = Lists.newArrayList();

        for (DicomObject r = fsRecord; r != null; r = dcd.findNextSiblingRecord(r)) try {
            DirectoryRecord rec = null;
            final DirectoryRecord.Type type = DirectoryRecord.Type.getInstance(r.getString(Tag.DirectoryRecordType));
            if (type == null) {
                logger.warn("Unrecognized DICOMDIR record type " + r.getString(Tag.DirectoryRecordType));
                continue;
            }

            String path = null;
            if (r.contains(Tag.ReferencedFileID)) {
                // dcm4che breaks fields on the same character used as pathname separator ('\')
                // this isn't such a bad thing, because we want to use the os-appropriate separator anyway
                final File file = new File(dir, StringUtils.join(r.getStrings(Tag.ReferencedFileID), File.separatorChar)).getCanonicalFile();
                assert file.getCanonicalPath().equals(file.getPath());
                if (file.exists()) {
                    path = file.getPath();
                    if (DirectoryRecord.Type.INSTANCE.equals(type)) {
                        files.add(file);
                    }
                }
            }

            if (factory != null) {
                // Extract the available information from the DICOMDIR
                final Map<Integer,String> values = Maps.newHashMap();
                for (final int tag : factory.getSelectionKeys(type)) {
                    values.put(tag, (tag == Tag.ReferencedFileID) ? path : r.getString(tag));
                }

                rec = factory.newInstance(type, upper, values);

                if (DirectoryRecord.Type.PATIENT.equals(type)) {
                    if (upper != null)
                        throw new IOException("PATIENT directory record entry not at top level in DICOMDIR");
                    patients.add(rec);
                } else if (upper == null) {
                    throw new IOException(type + " directory record entry without upper level in DICOMDIR");
                }
            }

            final DicomObject c = dcd.findFirstChildRecord(r);
            if (c != null) {
                files.addAll(readFileSetRecords(dir, dcd, c, rec));
            }
        } catch (IOException e) {	// IOException is bad news for our ability to continue
            throw e;
        } catch (Exception e) {	// Other exceptions are troubling but not necessarily disasters
            logger.error("Error reading DICOMDIR " + dir.getPath() + " directory record: " + e.getMessage());
            logger.error("Record object: " + r);
        }

        return files;
    }


    /**
     * Walks the given files to find DICOM files.
     * If a directory contains a DICOMDIR, we stop descending that way
     * and assume that the DICOMDIR catalogs all DICOM files.
     * @param dir root directory for search
     * @throws IOException
     */
    private synchronized void findDataFiles(final File[] infiles, final ProgressMonitorI pn)
    throws IOException,SQLException {
        final Set<File> directories = Sets.newLinkedHashSet();
        final List<File> files = Lists.newArrayList();
        int progress = 0;

        if (pn != null) {
            pn.setMinimum(0);
            pn.setMaximum(infiles.length);
            pn.setProgress(progress);
            pn.setNote("Scanning directories...");    // TODO: localize
        }

        for (final File file : infiles) {
            final File cfile = file.getCanonicalFile();
            if (cfile.isDirectory()) {
                directories.add(cfile);
                roots.add(cfile);
            } else if (DICOMDIR.equals(cfile.getName())) {
                // If this is a DICOMDIR, it indexes lots of files.
                final DicomDirReader dcd = new DicomDirReader(cfile);
                final File dir = cfile.getParentFile();
                files.addAll(readFileSetRecords(dir, dcd, dcd.findFirstRootRecord(), null));
                dcd.close();
                roots.add(dir);
            } else {
                files.add(cfile);
                roots.add(cfile.getParentFile());
            }
            if (pn != null) {
                pn.setMaximum(directories.size() + files.size());
                pn.setProgress(progress);
            }
        }

        // Walk through the given directories and all their subdirectories.
        // Because directories is a LinkedHashSet, the iterator is FIFO (like a Queue),
        // but add()ing a directory that's already present doesn't add a second instance.
        for (Iterator<File> di = directories.iterator(); di.hasNext(); ) {
            final File dir = di.next();
            di.remove();
            assert dir.isDirectory();
            assert dir.getPath().equals(dir.getCanonicalPath());	// we add only canonical paths

            if (pn != null) {
                pn.setNote(dir.getPath());
            }

            boolean dirsAdded = false;

            final File dcmdir = new File(dir, DICOMDIR);
            if (dcmdir.exists()) {
                // If there's a DICOMDIR in this directory, assume that it indexes all files
                // in this directory and its subdirectories.
                final DicomDirReader dcd = new DicomDirReader(dcmdir);
                files.addAll(readFileSetRecords(dir, dcd, dcd.findFirstRootRecord(), null));
                dcd.close();
            } else {
                for (final File file : dir.listFiles()) {
                    final File cfile = file.getCanonicalFile();
                    if (file.isDirectory()) {
                        // Try not to follow symbolic links (this is an imperfect kludge)
                        if (cfile.getPath().equals(file.getAbsolutePath())) {
                            directories.add(cfile);
                            dirsAdded = true;
                        }
                    } else {
                        files.add(cfile);
                    }
                }

                if (pn != null) {
                    pn.setMaximum(directories.size() + files.size());
                    pn.setProgress(progress);
                }

                // If we've added any directories, need to rebuild the iterator.
                if (dirsAdded) {
                    di = directories.iterator();
                }
            }
        }

        if (pn != null)
            pn.setMaximum(files.size());

        for (final File file : files) {
            if (pn != null) {
                if (pn.isCanceled()) return;
                pn.setNote(file.getPath());
            }

            assert !file.isDirectory();
            assert file.getPath().equals(file.getCanonicalPath());	// we add only canonical paths

            // Don't read a file more than once.
            if (contains(file)) {
                if (pn != null) {
                    pn.setProgress(++progress);
                }
                continue;
            }

            // Try reading the file as a DICOM object.
            final DataSetAttrs attrs;
            try {
                attrs = new DataSetAttrs(file, recordTags);
            } catch (IOException e) {
                if (pn != null) pn.setProgress(++progress);
                continue;   // not a DICOM object; move on to next file
            }

            // Add this file to the db
            qPathAddRow.setString(1, file.getPath());
            qPathAddRow.setString(2, attrs.getString(Tag.SOPClassUID));
            qPathAddRow.setString(3, attrs.getString(Tag.TransferSyntaxUID, DEFAULT_TS_UID));
            final int updated = qPathAddRow.executeUpdate();
            assert 1 == updated;

            if (factory != null) {
                // build a Patient record; if a match already exists, use that instead.
                boolean isNewPatient = true;
                final Map<Integer,String> patientValues = Maps.newHashMap();
                for (final int tag : factory.getSelectionKeys(DirectoryRecord.Type.PATIENT)) try {
                    patientValues.put(tag, attrs.get(tag));
                } catch (ConversionFailureException e) {}

                DirectoryRecord patient = factory.newInstance(DirectoryRecord.Type.PATIENT, null, patientValues);
                for (final DirectoryRecord p : patients)
                    if (p.equals(patient)) { patient = p; isNewPatient = false; break; }
                if (isNewPatient) patients.add(patient);

                // build the Study; we don't just set the parent in the constructor because
                // we might reuse an older instance instead, in which case this Study object
                // will be discarded, and we don't want the Patient to have a reference to it.
                final Map<Integer,String> studyValues = Maps.newHashMap();
                for (final int tag : factory.getSelectionKeys(DirectoryRecord.Type.STUDY)) try {
                    studyValues.put(tag, attrs.get(tag));
                } catch (ConversionFailureException e) {}
                DirectoryRecord study = factory.newInstance(DirectoryRecord.Type.STUDY, null, studyValues);
                for (DirectoryRecord s : patient.getLower())
                    if (s.equals(study)) { study = s; break; }
                study.setUpper(patient);

                // build the Series (no Study in constructor: see note for Study above)
                final Map<Integer,String> seriesValues = Maps.newHashMap();
                for (final int tag : factory.getSelectionKeys(DirectoryRecord.Type.SERIES)) try {
                    seriesValues.put(tag, attrs.get(tag));
                } catch (ConversionFailureException e) {}
                DirectoryRecord series = factory.newInstance(DirectoryRecord.Type.SERIES, null, seriesValues);
                for (DirectoryRecord s : study.getLower())
                    if (s.equals(series)) { series = s; break; }
                series.setUpper(study);

                // finally, build the Image (no Series in constructor: see note for Study above)
                final Map<Integer,String> imageValues = Maps.newHashMap();
                for (int tag : factory.getSelectionKeys(DirectoryRecord.Type.INSTANCE)) try {
                    imageValues.put(tag, attrs.get(tag));
                } catch (ConversionFailureException e) {}
                imageValues.put(Tag.ReferencedFileID, file.getPath());

                DirectoryRecord image = factory.newInstance(DirectoryRecord.Type.INSTANCE, null, imageValues);
                for (DirectoryRecord i : series.getLower())
                    if (i.equals(image)) { image = i; break; }
                image.setUpper(series);
            }

            if (pn != null) {
                pn.setProgress(++progress);
            }
        }
    }

    private boolean isCacheComplete(final int tag) {
        try {
            final Statement s = db.createStatement();
            try {
                final String col = tagToCol.get(tag);
                if (col == null) {
                    return false;
                }
                final ResultSet rs = s.executeQuery("SELECT COUNT(" + col + ") FROM " + tableName);
                try {
                    rs.next();
                    final int count = rs.getInt(1);

                    final int size = size();
                    assert count <= size : "too many cache entries: found " + count + ", expected <= " + size;
                    return count == size;
                } finally {
                    rs.close();
                }
            } finally {
                s.close();
            }
        } catch (SQLException e) {
            logger.error("SQL exception while checking cache completeness: " + e.getMessage());
            return false;
        }

    }

    private boolean isCacheComplete(final File f, final Integer...tags) {
        assert tags.length == 1 || tags.length == 2;
        final int[] tagRange = {tags[0], tags.length == 2 ? tags[1] : tags[0]};
        if (declaredComplete.containsKey(f)) {
            int[] completeRange = declaredComplete.get(f);
            return completeRange[0] <= tagRange[0] && tagRange[1] <= completeRange[1];
        } else return false;
    }


    /**
     * Check to see if this file has already been declared complete for
     * some range; if so, and the old and new ranges overlap, we can
     * declare the union complete (rather than just the new).
     * If the ranges don't overlap, just assign the new.
     * @param f File for which we are declaring completeness
     * @param tags minimum and (optionally) maximum tags
     */
    private synchronized void declareCacheComplete(final File f, final Integer...tags) {
        assert tags.length == 1 || tags.length == 2;
        final int[] tagRange = {tags[0], tags.length == 2 ? tags[1] : tags[0]};
        if (declaredComplete.containsKey(f)) {
            final int[] prevRange = declaredComplete.get(f);
            if (prevRange[0] < tagRange[0] && tagRange[0] <= prevRange[1])
                tagRange[0] = prevRange[0];
            if (prevRange[0] <= tagRange[1] && tagRange[1] <= prevRange[1])
                tagRange[1] = prevRange[1];
        }
        declaredComplete.put(f, tagRange);
    }


    private void cacheAttrs(final File file, final DataSetAttrs attrs,
            final Map<Integer,ConversionFailureException> failed)
    throws SQLException {
        final String path = file.getPath();
        try {
            assert path.equals(file.getCanonicalPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        for (final int tag : attrs) {
            if (!tagToCol.containsKey(tag)) {      // no column for this attribute; add one.
                String col = String.format(COLUMN_FORMAT, attrs.getVR(tag), tag);
                tagToCol.put(tag, col);
                final Statement s = db.createStatement();
                try {
                    final StringBuilder alter = new StringBuilder("ALTER TABLE ");
                    alter.append(tableName).append(" ADD COLUMN ").append(col).append(" VARCHAR");
                    s.execute(alter.toString());
                    for (SQLWarning warning = s.getWarnings(); warning != null; warning = warning.getNextWarning()) {
                        logger.warn(warning.toString());
                    }
                } finally {
                    s.close();
                }
            }
        }

        final StringBuilder sb = new StringBuilder("UPDATE ");
        sb.append(tableName).append(" SET ");

        final Set<String> assignments = Sets.newHashSet();
        for (final int tag : attrs) {
            try {
                String val = attrs.get(tag);
                if (null != val) {
                    if (0 < maxValueLength && maxValueLength < val.length()) {
                        val = String.format(valueTruncateFormat, val.substring(0, maxValueLength));
                    }
                    assignments.add(String.format("%1$s='%2$s'", tagToCol.get(tag), StringEscapeUtils.escapeSql(val)));
                }
            } catch (ConversionFailureException e) {
                failed.put(tag, e);
            }
        }

        if (assignments.isEmpty()) {
            return;
        }

        Joiner.on(", ").appendTo(sb, assignments);
        sb.append(" WHERE path='").append(path).append("'");

        final Statement s = db.createStatement();
        try {
            final int updated = s.executeUpdate(sb.toString());
            assert 1 == updated;
        } finally {
            s.close();
        }
    }

    private void cache(final File file, final Collection<Integer> tags,
            final Map<Integer,ConversionFailureException> failed)
    throws IOException,SQLException {
        cacheAttrs(file, new DataSetAttrs(file, Iterables.filter(tags, new Predicate<Integer>() {
            public boolean apply(Integer i) {
                return !isCacheComplete(file, i);
            }
        })), failed);
    }

    /**
     * Read all the named attributes from all files in the fileset, and
     * cache the results.
     * @param tags DICOM tags of all attributes of interest
     * @throws IOException
     * @throws SQLException
     */
    public void fillCache(final Collection<Integer> tags,
            final Map<Integer,ConversionFailureException> failed,
            final ProgressMonitorI pn)
    throws IOException,SQLException {
        final Set<File> files = getDataFiles();
        if (pn != null) {
            pn.setMinimum(0);
            pn.setMaximum(files.size());
            pn.setProgress(0);
        }

        final Collection<Integer> uncached = Collections2.filter(tags, new Predicate<Integer>() {
            public boolean apply(Integer i) { return !isCacheComplete(i); }
        });
        if (uncached.isEmpty()) {
            return;
        }

        // Now load the needed attributes
        int progress = 0;
        for (final File file : files) {
            cache(file, uncached, failed);
            if (null != pn) {
                if (pn.isCanceled()) {
                    return;
                }
                pn.setProgress(++progress);
            }
        }
        if (pn != null) {
            pn.close();
        }
    }


    public void fillCache(Collection<Integer> tags, final Map<Integer,ConversionFailureException> failed)
    throws IOException,SQLException {
        fillCache(tags, failed, null);
    }


    public void cache(final File file, final int minTag, final int maxTag,
            final Map<Integer,ConversionFailureException> failed)
    throws IOException,SQLException {
        if (isCacheComplete(file, minTag, maxTag)) {
            return;
        }
        cacheAttrs(file, new DataSetAttrs(file, maxTag), failed);
        declareCacheComplete(file, minTag, maxTag);
    }

    /**
     * Read the named files, get all attributes in the indicated range,
     * and cache the results.
     * @param Files to read; if null, use all files in this file set
     * @param maxTag largest DICOM tag to cache
     */
    public void fillCache(Collection<File> files,
            final int minTag, final int maxTag,
            final Map<Integer,ConversionFailureException> failed,
            final ProgressMonitorI pn)
    throws IOException,SQLException {
        if (null == files) {
            files = getDataFiles();
        }

        if (pn != null) {
            pn.setMinimum(0);
            pn.setMaximum(files.size());
            pn.setProgress(0);
        }

        int progress = 0;
        for (final File file : files) {
            cache(file, minTag, maxTag, failed);
            if (pn != null) {
                if (pn.isCanceled()) {
                    return;
                }
                pn.setProgress(++progress);
            }
        }
        if (pn != null) pn.close();
    }

    public void fillCache(Collection<File> files, final int minTag, final int maxTag,
            final Map<Integer,ConversionFailureException> failed)
    throws IOException,SQLException {
        fillCache(files, minTag, maxTag, failed, null);
    }


    /**
     * Finds all files in the fileset that match all the given attribute values
     * @param values map of DICOM tag to attribute value
     * @return set of files matching all the given attribute values
     * @throws IOException
     */
    public Set<File> getFilesForValues(final Map<Integer,String> values,
            final Map<Integer,ConversionFailureException> failed)
            throws IOException,SQLException {
        if (values.isEmpty()) {       // special case: no constraints means all files
            return getDataFiles();
        }

        // First load all the requested attributes
        fillCache(values.keySet(), failed);

        final StringBuilder sb = new StringBuilder("SELECT path FROM ");
        sb.append(tableName);
        sb.append(" WHERE ");
        Joiner.on(" AND ").appendTo(sb,
                Iterables.transform(values.keySet(), new Function<Integer,String>() {
                    public String apply(final Integer i) {
                        return String.format("%1$s='%2$s'",
                                tagToCol.get(i),
                                StringEscapeUtils.escapeSql(values.get(i)));
                    }
                }));

        final Statement s = db.createStatement();
        try {
            final ResultSet rs = s.executeQuery(sb.toString());
            try {
                final Set<File> files = Sets.newLinkedHashSet();
                while (rs.next()) {
                    final String path = rs.getString(1);
                    if (rs.wasNull()) throw new IOException("NULL path value in cache");
                    assert path != null;
                    files.add(new File(path));
                }
                return files;
            } finally {
                rs.close();
            }
        } finally {
            s.close();
        }
    }


    /**
     * Returns all combinations of values for the given attributes in the file set.
     * @param tags tags for the attributes of interest
     * @return A Set of tag->value Maps, each Map describing one combination.
     */
    public Set<Map<Integer,String>> getUniqueCombinations(final Collection<Integer> tags,
            final Map<Integer,ConversionFailureException> failed)
            throws IOException,SQLException {
        return getUniqueCombinationsGivenValues(Maps.<Integer,String>newHashMap(), tags, failed);
    }

    /**
     * @see #getUniqueCombinations(Set)
     * @throws IOException
     * @throws SQLException
     */
    public Set<Map<Integer,String>> getUniqueCombinations(final Map<Integer,ConversionFailureException> failed,
            final Integer... tags)
            throws IOException,SQLException {
        return getUniqueCombinationsGivenValues(Collections.<Integer,String>emptyMap(),
                Arrays.asList(tags), failed);
    }


    /**
     * Return all unique combinations of values in the fileset for both
     * the given and requested attributes, given the constrains.
     * @param given map of tag->value constraints
     * @param requested	set of tags of requested attributes
     * @return set of maps, each map holding a tag->value combination
     * @throws IOException
     * @throws SQLException
     */
    public Set<Map<Integer,String>> getUniqueCombinationsGivenValues(
            final Map<Integer,String> given, final Collection<Integer> requested,
            final Map<Integer,ConversionFailureException> failed)
            throws IOException,SQLException {
        if (requested.isEmpty()) {
            return Collections.emptySet();
        }

        // All the given are presumably already cached.
        // (apparently I'm not ready to assert this yet)
        final Set<Integer> allTags = Sets.newHashSet(requested);
        allTags.addAll(given.keySet());
        fillCache(allTags, failed);

        final SortedSet<Integer> tags = Sets.newTreeSet(requested);

        StringBuilder sb = new StringBuilder("SELECT DISTINCT ");
        Joiner.on(", ").appendTo(sb,
                Iterables.transform(tags, new Function<Integer,String>() {
                    public String apply(final Integer i) {
                        return tagToCol.get(i);
                    }
                }));

        sb.append(" FROM ");
        sb.append(tableName);

        // Construct constraint, if any
        if (!given.isEmpty()) {
            sb.append(" WHERE ");
            Joiner.on(" AND ").appendTo(sb,
                    Iterables.transform(given.keySet(), new Function<Integer,String>() {
                        public String apply(final Integer i) {
                            return String.format("%1$s='%2$s'", tagToCol.get(i), given.get(i));

                        }
                    }));
        }

        final Set<Map<Integer,String>> combs = Sets.newHashSet();
        final Statement s = db.createStatement();
        try {
            final ResultSet rs = s.executeQuery(sb.toString());
            try {
                while (rs.next()) {
                    final Map<Integer,String> vals = Maps.newHashMap();
                    int i = 1;
                    for (final int tag : tags) {
                        final String val = rs.getString(i++);
                        if (val != null) {
                            vals.put(tag,val);
                        }
                    }
                    combs.add(vals);
                }
            } finally {
                rs.close();
            }
        } finally {
            s.close();
        }

        return combs;
    }


    /**
     * Returns all values for the given attribute in the file set.
     * @param tag tag for attribute of interest
     * @return All values for the indicated attribute in the file set
     * @throws IOException
     */
    public Set<String> getUniqueValues(final int tag)
    throws IOException,ConversionFailureException,SQLException {
        final Map<Integer,ConversionFailureException> failed = new HashMap<Integer,ConversionFailureException>();
        final SetMultimap<Integer,String> values = getUniqueValues(Collections.singleton(tag), failed);
        if (failed.isEmpty()) {
            return values.get(tag);
        } else {
            throw failed.get(tag);
        }
    }


    /**
     * Returns all values for the given attributes in the file set
     * @param tags
     * 			The DICOM tags for the requested attributes
     * @return
     * 			All values for the given attributes in this file set
     * @throws IOException
     */
    public SetMultimap<Integer,String> getUniqueValues(final Collection<Integer> tags,
            final Map<Integer,ConversionFailureException> failed)
            throws IOException,SQLException {
        final SetMultimap<Integer,String> values = HashMultimap.create();

        // First load all the requested attributes
        fillCache(tags, failed);

        try {
            final Statement s = db.createStatement();
            try {
                for (final int tag : tags) {
                    final ResultSet rs = s.executeQuery(String.format("SELECT DISTINCT %1$s FROM %2$s",
                            tagToCol.get(tag), tableName));
                    try {
                        while (rs.next()) {
                            values.put(tag, rs.getString(1));
                        }
                    } finally {
                        rs.close();
                    }
                }
                return values;
            } finally {
                s.close();
            } 
        } catch (SQLException e) {
            throw new IOException("error accessing hsqldb: " + e.getMessage());
        }
    }


    /**
     * @see #getUniqueValues(Set)
     * @throws IOException
     * @throws SQLException
     */
    public SetMultimap<Integer,String> getUniqueValues(final Map<Integer,ConversionFailureException> failed,
            final Integer... tags)
            throws IOException,SQLException {
        return getUniqueValues(Arrays.asList(tags), failed);
    }

    /**
     * Return all unique values for the requested attributes in the given files.
     * @param files Set of files from which attribute values are requested
     * @param tags DICOM tags of attributes to be examined
     * @param cacheKnownComplete true if the cache is known to be complete for
     *    these attributes in these files (e.g., if we just filled the cache)
     * @return Map from DICOM tag to set of values
     * @throws IOException
     * @throws SQLException
     */
    private SetMultimap<Integer,String>
    getUniqueValuesFromFiles(final Set<File> files, final Collection<Integer> requested,
            final Map<Integer,ConversionFailureException> failed,
            final boolean cacheKnownComplete)
            throws IOException,SQLException {
        final SetMultimap<Integer,String> vals = HashMultimap.create();
        if (files.isEmpty() || requested.isEmpty()) {
            return vals;
        }

        // make a sorted local copy to ensure iteration order and to get min and max
        final SortedSet<Integer> tags = Sets.newTreeSet(requested);
        if (!cacheKnownComplete) {
            fillCache(files, tags.first(), tags.last(), failed);
        }

        final StringBuilder sb = new StringBuilder("SELECT path, ");
        Joiner.on(", ").appendTo(sb,
                Iterables.transform(tags, new Function<Integer,String>() {
                    public String apply(final Integer i) {
                        return tagToCol.get(i);
                    }
                }));
        sb.append(" FROM ").append(tableName);

        final Statement s = db.createStatement();
        try {
            final ResultSet rs = s.executeQuery(sb.toString());
            try {
                while (rs.next()) {
                    final File file = new File(rs.getString(1));
                    if (files.contains(file)) {
                        int i = 2;
                        for (final int tag : tags) {
                            vals.put(tag, rs.getString(i++));
                        }
                    }
                }
            } finally {
                rs.close();
            }
        } finally {
            s.close();
        }

        return vals;
    }


    /**
     * Returns all unique values for the requested attributes in the named files
     * @param files Set of files from which attribute values are requested
     * @param tags DICOM tags of attributes to be examined
     * @return Map from DICOM tag to set of values
     * @throws IOException
     * @throws SQLException
     */
    public SetMultimap<Integer,String>
    getUniqueValuesFromFiles(final Set<File> files, final Collection<Integer> tags,
            final Map<Integer,ConversionFailureException> failed)
            throws IOException,SQLException {
        return getUniqueValuesFromFiles(files, tags, failed, false);
    }


    /**
     * Returns all values for the requested attributes in files for which the
     * given attributes have the given values.
     * @param given Map of attribute values used as constraint
     * @param tags Set of tags for which we want values
     * @return Map of requested attribute values, given the constraint
     * @throws IOException
     */
    public SetMultimap<Integer,String>
    getUniqueValuesGivenValues(final Map<Integer,String> given, final Set<Integer> tags,
            final Map<Integer,ConversionFailureException> failed)
            throws IOException,SQLException {
        // All the given are presumably already cached.
        // (apparently I'm not ready to assert this yet)
        fillCache(Sets.union(tags, given.keySet()), failed);
        return getUniqueValuesFromFiles(getFilesForValues(given, failed), tags, failed, true);
    }


    /**
     * Returns values for all attributes in the given tag range in
     * the given file
     * @param file
     * @param minTag
     * @param maxTag
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public Map<Integer,String>
    getValuesFromFile(final File file, final int minTag, final int maxTag,
            final Map<Integer,ConversionFailureException> failed)
            throws IOException,SQLException {
        final Map<Integer,String> vals = new HashMap<Integer,String>();

        cache(file, minTag, maxTag, failed);

        // convert the range to an ordered set of tags
        final SortedSet<Integer> tags = Sets.newTreeSet();
        for (final int tag : tagToCol.keySet()) {
            if (minTag <= tag && tag <= maxTag) {
                tags.add(tag);
            }
        }

        final StringBuilder sb = new StringBuilder("SELECT ");

        // build comma-separated list of column names for requested tags
        Joiner.on(", ").appendTo(sb,
                Iterables.transform(Iterables.filter(tags,
                        new Predicate<Integer>() {
                    public boolean apply(final Integer i) {
                        return minTag <= i && i <= maxTag;
                    }
                }), new Function<Integer,String>() {
                    public String apply(final Integer i) {
                        return tagToCol.get(i);
                    }
                }));

        sb.append(" FROM ").append(tableName);
        sb.append(" WHERE path='").append(file.getPath()).append("'");

        try {
            final Statement s = db.createStatement();
            try {
                final ResultSet rs = s.executeQuery(sb.toString());
                try {
                    if (rs.next()) {
                        int col = 1;
                        for (final int tag : tags) {    // iteration order must be same as above
                            vals.put(tag, rs.getString(col++));
                        }
                        if (rs.next()) {
                            logger.warn("Multiple rows found for " + file.getPath());
                        }
                    } else {
                        logger.error(file.getPath() + " was cached but is missing from database");
                    }
                } finally {
                    rs.close();
                }
            } finally {
                s.close();
            }
        } catch (SQLException e) {
            throw new IOException("error accessing hsqldb: " + e.getMessage());
        }
        return vals;
    }


    /**
     * Returns all unique values for all attributes in the given tag range in the
     * named files, after ensuring that the values are cached.
     * 
     * @param files Files to look for values
     * @param minTag Minimum tag to return
     * @param maxTag Maximum tag to return
     * @param pns ProgressMonitorI objects to monitor progress
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public Map<Integer,Set<String>>
    getUniqueValuesFromFiles(final Set<File> files, final int minTag, final int maxTag,
            final Map<Integer,ConversionFailureException> failed,
            final ProgressMonitorI pn)
            throws IOException,SQLException {
        int progress = 0;
        if (pn != null) {
            pn.setMinimum(0);
            pn.setProgress(progress);
            pn.setMaximum(files.size());
        }

        final Map<Integer,Set<String>> vals = new HashMap<Integer,Set<String>>();
        for (final File file : files) {
            final Map<Integer,String> fv = getValuesFromFile(file, minTag, maxTag, failed);
            for (Map.Entry<Integer,String> e : fv.entrySet()) {
                final int tag = e.getKey();
                if (!vals.containsKey(tag)) {
                    vals.put(tag, Sets.<String>newHashSet());
                }
                vals.get(tag).add(e.getValue());
            }
            if (pn != null)
                pn.setProgress(++progress);
        }

        return vals;
    }


    public Map<Integer,Set<String>>
    getUniqueValuesFromFiles(final Set<File> files, final int minTag, final int maxTag,
            final Map<Integer,ConversionFailureException> failed)
            throws IOException,SQLException {
        fillCache(files, minTag, maxTag, failed, null);
        return getUniqueValuesFromFiles(files, minTag, maxTag, failed, null);
    }
    
    public boolean isSequenceTag(final int tag) {
        return tagToCol.containsKey(tag) && tagToCol.get(tag).startsWith("SQ");
    }
}