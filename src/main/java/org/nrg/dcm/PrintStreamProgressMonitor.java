/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm;

import java.io.PrintStream;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class PrintStreamProgressMonitor implements ProgressMonitorI {
  private final PrintStream out;
  private String note = "";
  @SuppressWarnings("unused")
  private int min = 0, max = 0;
  
  /**
   * 
   */
  public PrintStreamProgressMonitor(final PrintStream out) {
    this.out = out;
  }

  /* (non-Javadoc)
   * @see org.nrg.dcm.ProgressMonitorI#close()
   */
  public void close() {
    out.println(note + " complete");
  }

  /* (non-Javadoc)
   * @see org.nrg.dcm.ProgressMonitorI#isCanceled()
   */
  public boolean isCanceled() { return false; }

  /* (non-Javadoc)
   * @see org.nrg.dcm.ProgressMonitorI#setMaximum(int)
   */
  public void setMaximum(final int max) { this.max = max; }

  /* (non-Javadoc)
   * @see org.nrg.dcm.ProgressMonitorI#setMinimum(int)
   */
  public void setMinimum(final int min) { this.min = min; }

  /* (non-Javadoc)
   * @see org.nrg.dcm.ProgressMonitorI#setNote(java.lang.String)
   */
  public void setNote(final String note) {
    this.note = note;
  }

  /* (non-Javadoc)
   * @see org.nrg.dcm.ProgressMonitorI#setProgress(int)
   */
  public void setProgress(final int current) {
    out.println(note + " (" + current + "/" + max + ")");
  }
}
