/**
 * Copyright (c) 2006-2009,2011 Washington University
 */
package org.nrg.dcm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.zip.GZIPInputStream;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.StopTagInputHandler;
import org.dcm4che2.util.StringUtils;
import org.dcm4che2.util.TagUtils;
import org.nrg.attr.ConversionFailureException;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * Manages reading DICOM files and retrieving attribute values.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class DataSetAttrs implements Iterable<Integer> {
    private final static String GZIP_SUFFIX = ".gz";

    private final DicomObject o;
    private final int maxTag;
    private final SortedSet<Integer> tagSet;

    /**
     * @param file DICOM file from which attributes are to be read
     * @param maxTag largest tag value of attributes to be read
     * @throws IOException if file is empty or not a DICOM file
     */
    DataSetAttrs(final File file, final int maxTag) throws IOException {
        this.maxTag = maxTag;
        o = read(file, maxTag);
        tagSet = Sets.newTreeSet();
        for (final Iterator<DicomElement> ei = o.datasetIterator(); ei.hasNext(); ) {
            tagSet.add(ei.next().tag());
        }
    }

    DataSetAttrs(final File file, final Iterable<Integer> tags) throws IOException {
        tagSet = Sets.newTreeSet(tags);
        if (tagSet.isEmpty()) {
            maxTag = -1;
            o = new BasicDicomObject();
        } else {
            maxTag = tagSet.last();
            o = read(file, maxTag);
        }
    }

    DataSetAttrs(final File file) throws IOException {
        this(file,Integer.MAX_VALUE);
    }


    public String toString() {
        return Joiner.on(";").join(Iterables.transform(tagSet, new Function<Integer,String>() {
            public String apply(Integer i) { return TagUtils.toString(i); }
        }));
    }

    private static DicomObject read(final File file, final int maxTag)
            throws IOException {
        IOException ioexception = null;
        InputStream fin = new FileInputStream(file);
        try {
            if (file.getName().endsWith(GZIP_SUFFIX)) {
                fin = new GZIPInputStream(fin);
            }
            final BufferedInputStream bin = new BufferedInputStream(fin);
            try {
                final DicomInputStream in = new DicomInputStream(bin);
                try {
                    if (maxTag > 0) {
                        // Always load the SOP Class UID, at least.
                        in.setHandler(new StopTagInputHandler(1 + Math.max(maxTag, Tag.SOPClassUID)));
                    }
                    final DicomObject o = in.readDicomObject();
                    if (o.contains(Tag.FileMetaInformationVersion)) {
                        // looks part 10 compliant; assume it's valid
                        return o;
                    } else if (o.contains(Tag.SOPClassUID)) {
                        // non-part-10 compliant file has SOP Class UID set; it's
                        // probably okay.
                        return o;
                    } else {
                        throw new IOException(file.getPath()
                                + " is not a valid DICOM object: no SOP class UID");
                    }
                } catch (IOException e) {
                    throw ioexception = e;
                } catch (Throwable t) {
                    final IOException e = new IOException(file.getPath() + " could not be read");
                    e.initCause(t);
                    throw e;
                } finally {
                    try {
                        in.close();
                    } catch (IOException e) {
                        throw ioexception = null == ioexception ? e : ioexception;
                    }
                }
            } finally {
                try {
                    bin.close();
                } catch (IOException e) {
                    throw ioexception = null == ioexception ? e : ioexception;
                }
            }
        } finally {
            try {
                fin.close();
            } catch (IOException e) {
                throw ioexception = null == ioexception ? e : ioexception;
            }
        }
    }

    private interface Converter {
        String convert(DicomObject o, DicomElement e) throws ConversionFailureException;
    }

    private final static Map<VR,Converter> conversions = new HashMap<VR,Converter>();
    static {
        conversions.put(VR.SQ, new Converter() {
            public String convert(final DicomObject o, final DicomElement e) {
                return null;
            }
        });

        conversions.put(VR.UN, new Converter() {
            public String convert(final DicomObject o, final DicomElement e) {
                return e.getString(o.getSpecificCharacterSet(), false);
            }
        });

        conversions.put(VR.AT, new Converter() {
            public String convert(final DicomObject o, final DicomElement e) {
                final int[] vals = e.getInts(false);
                final StringBuilder sb = new StringBuilder();
                if (vals.length > 0) {
                    sb.append(TagUtils.toString(vals[0]));
                }
                for (int i = 1; i < vals.length; i++) {
                    sb.append('\\').append(TagUtils.toString(vals[i]));
                }
                return sb.toString();
            }
        });

        conversions.put(VR.OB, new Converter() {
            public String convert(final DicomObject o, final DicomElement e) {
                return VR.OB.toString(e.getBytes(), o.bigEndian(), o.getSpecificCharacterSet());
            }
        });
    }

    /**
     * Extracts a string representation of the given tag from the given DICOM object
     * @param o DICOM object
     * @param tag attribute tag
     * @return string representation of the value, or null if not present in the given object
     * @throws ConversionFailureException if no string representation is available for the attribute's VR
     */
    public static String convert(final DicomObject o, final int tag) throws ConversionFailureException {
        final DicomElement de = o.get(tag);
        if (null == de) return null;
        final VR vr = de.vr();
        if (conversions.containsKey(vr)) {
            return conversions.get(vr).convert(o, de);
        } else {
            // all other data types can be treated as simple strings, maybe with
            // multiple values separated by backslashes.  Join these.
            try {
                return StringUtils.join(de.getStrings(o.getSpecificCharacterSet(), false), '\\');
            } catch (UnsupportedOperationException e) {
                throw new ConversionFailureException(tag, o.getBytes(tag), "conversion failed for " +
                        TagUtils.toString(tag) + " VR " + vr, e);
            }
        }
    }

    public VR getVR(final int tag) {
        return o.vrOf(tag);
    }

    public String getString(final int tag) {
        return o.getString(tag);
    }

    public String getString(final int tag, final String defaultValue) {
        return o.getString(tag, defaultValue);
    }

    /**
     * Retrieves the value of the indicated attribute from this data set. Performs
     * conversions for a few selected types: VR TM -> xs:time, VR DA -> xs:date
     * @param tag attribute for which value is requested
     * @return attribute value
     * @throws ConversionFailureException
     */
    public String get(final int tag) throws ConversionFailureException {	
        if (tag > maxTag) {
            throw new IndexOutOfBoundsException("Cannot retrieve tag " + TagUtils.toString(tag)
                    + " from object with maximum tag " + TagUtils.toString(maxTag));
        }
        return convert(o, tag);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    public Iterator<Integer> iterator() {
        return Collections.unmodifiableCollection(tagSet).iterator();
    }
}